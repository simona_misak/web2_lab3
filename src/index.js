// Deklaracije
const localStorageKey = "asteroidsBestTime";

const numOfAsteroids = 5; // Broj objekata asteroida
const asteroidFrequency = 2000; // Učestalost generiranja asteroida

const playerMovementAmount = 5; // Pomak igrača
const keys = {}; // Spremanje pritisaka za obradu u glavnoj petlji

const gameTimeFont = "16px Arial";
const gameTimeColor = "black";

let player;
let asteroids = [];

let asteroidInterval = null;

let startTime;
let currentTime;

// Prostor igre
var myGameArea = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.id = "gameCanvas";

        // Cijeli prozor web preglednika
        this.canvas.width = window.innerWidth - 10;
        this.canvas.height = window.innerHeight - 10;

        this.ctx = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.frameNo = 0;

        // Podatak o najboljem vremenu pohranjuje se u local storage
        this.bestTime = parseFloat(localStorage.getItem(localStorageKey)) || 0;

        this.gameTimeX = this.canvas.width - 200;
        this.gameTimeY = 20;
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

// Objekt igrača
function Player() {
    this.width = 50;
    this.height = 30;

    // Postavljanje na sredinu
    this.x = myGameArea.canvas.width / 2 - this.width / 2;
    this.y = myGameArea.canvas.height / 2 - this.height / 2;
    this.color = "red";

    this.draw = function() {
        // Bojanje igrača
        myGameArea.ctx.fillStyle = this.color;
        myGameArea.ctx.fillRect(this.x, this.y, this.width, this.height);

        // 3D sjena
        myGameArea.ctx.shadowBlur = 10;
        myGameArea.ctx.shadowColor = "black";

        // Obrub
        myGameArea.ctx.strokeStyle = "darkred";
        myGameArea.ctx.lineWidth = 1;
        myGameArea.ctx.strokeRect(this.x, this.y, this.width, this.height);
    };
}

// Generiranje različitih nijansi sive boje
function generateRandomGray() {
    const grayShade = Math.floor(Math.random() * 156) + 100; // Odabir broja između 100 i 255
    return `rgb(${grayShade}, ${grayShade}, ${grayShade})`;
}

// Objekt asteroida
function Asteroid(x, y, width, height, speed, direction) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = generateRandomGray();
    this.speed = speed;
    this.direction = direction;

    this.draw = function() {
        // Bojanje asteroida
        myGameArea.ctx.fillStyle = this.color;
        myGameArea.ctx.fillRect(this.x, this.y, this.width, this.height);

        // 3D sjena
        myGameArea.ctx.shadowBlur = 10;
        myGameArea.ctx.shadowColor = "black";

        // Obrub
        myGameArea.ctx.strokeStyle = "black";
        myGameArea.ctx.lineWidth = 1;
        myGameArea.ctx.strokeRect(this.x, this.y, this.width, this.height);
    };

    this.update = function() {
        this.x -= this.speed * Math.cos(this.direction);
        this.y -= this.speed * Math.sin(this.direction);

        if (this.x + this.width < 0 || this.y + this.height < 0) {
            this.reset();
        }
    };

    this.reset = function() {
        this.x = myGameArea.canvas.width + Math.random() * 200;
        this.y = Math.random() * (myGameArea.canvas.height - this.height);
        this.direction = Math.random() * Math.PI * 2;
    };
}

// Funkcija za detekciju kolizije
function isCollision(player, asteroid) {
    return (
        player.x < asteroid.x + asteroid.width &&
        player.x + player.width > asteroid.x &&
        player.y < asteroid.y + asteroid.height &&
        player.y + player.height > asteroid.y
    );
}

// Funkcija za prikaz vremena
function drawGameTime() {
    myGameArea.ctx.font = gameTimeFont;
    myGameArea.ctx.fillStyle = gameTimeColor;

    // Pretvara najbolje vrijeme u format minute:sekunde.milisekunde
    const bestMinutes = Math.floor(myGameArea.bestTime / 60000);
    const bestSeconds = ((myGameArea.bestTime % 60000) / 1000).toFixed(3);

    myGameArea.ctx.fillText(`Best Time: ${bestMinutes}:${bestSeconds}`, myGameArea.gameTimeX, myGameArea.gameTimeY);

    // Pretvara proteklo vrijeme u format minute:sekunde.milisekunde
    const minutes = Math.floor(currentTime / 60000);
    const seconds = ((currentTime % 60000) / 1000).toFixed(3);

    myGameArea.ctx.fillText(`Currrent Time: ${minutes}:${seconds}`, myGameArea.gameTimeX, myGameArea.gameTimeY + 20);
}

// Inicijalizacija igre
function initGame() {
    player = new Player();
    asteroids = [];
    startTime = Date.now(); // Vrijeme pokretanja igre

    // Generiranje asteroida
    function createAsteroid() {
        for (let i = 0; i < numOfAsteroids; i++) {
            let side = Math.floor(Math.random() * 4); // Slučajan odabir strane ekrana (0, 1, 2 ili 3)
            let x;
            let y;
            
            // Postavljanje početne pozicije asteroida ovisno o odabranoj strani (pozicija izvan ekrana)
            switch (side) {
                case 0:
                    x = Math.random() * myGameArea.canvas.width;
                    y = -30;
                    break;
                case 1:
                    x = myGameArea.canvas.width + 30;
                    y = Math.random() * myGameArea.canvas.height;
                    break;
                case 2:
                    x = Math.random() * myGameArea.canvas.width;
                    y = myGameArea.canvas.height + 30;
                    break;
                case 3:
                     x = -30;
                     y = Math.random() * myGameArea.canvas.height;
                     break;
            }
            const asteroid = new Asteroid(
                x,
                y,
                Math.random() * (50 - 20) + 20, // Različita veličina asteroida
                Math.random() * (50 - 20) + 20,
                Math.random() * 3 + 1, // Slučajna brzina kretanja asteroida
                Math.random() * Math.PI * 2 // Slučajan smjer kretanja asteroida
            );
            asteroids.push(asteroid);
        }
    }
    // Za stvaranje novih asteroida
    asteroidInterval = setInterval(createAsteroid, asteroidFrequency);
}

// Glavna petlja igre
function gameLoop() {
    myGameArea.ctx.clearRect(0, 0, myGameArea.canvas.width, myGameArea.canvas.height);

    // Crta igrača
    movePlayer();
    player.draw();

    // Provjerava koliziju s asteroidima
    for (let i = 0; i < asteroids.length; i++) {
        asteroids[i].draw();
        asteroids[i].update();

        if (isCollision(player, asteroids[i])) {
            endGame();
            return;
        }
    }

    currentTime = Date.now() - startTime; // Proteklo vrijeme od pokretanja igre
    // Postavljanje novog najboljeg vremena
    if (currentTime > myGameArea.bestTime) {
        myGameArea.bestTime = currentTime;
        localStorage.setItem(localStorageKey, myGameArea.bestTime.toString());
    }

    // Crta informacije o vremenu
    drawGameTime();

    // Poziva samu sebe za idući frame
    requestAnimationFrame(gameLoop);
}

// Kretanje igrača
function movePlayer() {
    if (keys.ArrowLeft) {
        player.x -= playerMovementAmount;
    }
    if (keys.ArrowRight) {
        player.x += playerMovementAmount;
    }
    if (keys.ArrowDown) {
        player.y += playerMovementAmount;
    }
    if (keys.ArrowUp) {
        player.y -= playerMovementAmount;
    }
}

function keyEventHandler(event) {
    keys[event.code] = event.type === "keydown";
    event.preventDefault();
}

// Početak igre
function startGame() {
    myGameArea.start();

    // Slušači tipkovnice za kretanje igrača
    window.addEventListener("keydown", keyEventHandler);
    window.addEventListener("keyup", keyEventHandler);

    // Pokreće igru
    initGame();
    gameLoop();
}

// Završetak igre
function endGame() {
    clearInterval(asteroidInterval);
    initGame();
    gameLoop();
}